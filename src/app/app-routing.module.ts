import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { HistoryComponent } from './components/history/component/history.component';
import { LoginComponent } from './components/login/component/login.component';
import { AuthGuard } from './helpers/AuthGuard';

const routes: Routes = [
{ path: '', redirectTo: '/login', pathMatch: 'full' },
{ path: 'login', component: LoginComponent },
{ path: 'create', component: CreateUserComponent },
{ path: 'history', component: HistoryComponent, canActivate: [AuthGuard]},
{ path: '**', redirectTo: '/login', pathMatch: 'full' },
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
