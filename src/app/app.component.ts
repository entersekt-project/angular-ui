import { Component, HostListener, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  /**
	 * Night sky view child
	 *
	 * @type { ViewChild }
	 */
  @ViewChild('nightSky', { static: true }) nightSky: any;

  /**
	 * Mountain range view child
	 *
	 * @type { ViewChild }
	 */
  @ViewChild('mountainrange', { static: true }) mountainrange: any;
  
  constructor(){
    sessionStorage.removeItem("jwt");
  }

  @HostListener('document:mousemove', ['$event']) 
  onMouseMove(e:any) {
    let movwX = (e.pageX * -1 /15);
    let movwY = (e.pageY * -1 /15);
    this.nightSky.nativeElement.style.backgroundPositionX= movwX + "px";
    this.nightSky.nativeElement.style.backgroundPositionY = movwY + "px";
    let movwXx = (e.pageX * -1 /30);
    let movwYy = (e.pageY * -1 /30);
    this.mountainrange.nativeElement.style.backgroundPositionX= movwXx + "px";
    this.mountainrange.nativeElement.style.backgroundPositionY = movwYy + "px";
  }
}



