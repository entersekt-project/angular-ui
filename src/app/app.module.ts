import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { MaterialExampleModule } from 'material.module';
import { HistoryComponent } from './components/history/component/history.component';
import { CommonModule } from '@angular/common';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { LoginComponent } from './components/login/component/login.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { LoginService } from './components/login/services/login.service';
import { JwtInterceptor } from './helpers/JwtInterceptor.service';
import { AuthGuard } from './helpers/AuthGuard';

@NgModule({
  declarations: [
    AppComponent,
    HistoryComponent,
    CreateUserComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatInputModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    MaterialExampleModule,
    ReactiveFormsModule,     
    MatNativeDateModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MaterialExampleModule,
    ReactiveFormsModule,
    MatCheckboxModule
  ],
  providers: [
    LoginService,
    { provide: HTTP_INTERCEPTORS,
    useClass: JwtInterceptor,
    multi: true}
    ,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
