import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { LoginService } from '../login/services/login.service';

@Component({
  selector: 'create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent {
    /**
	 * Display password or text
	 *
	 * @type { boolean }
	 */
  public displayPassword: boolean = false;

  /**
	 * Input type
	 *
	 * @type { string }
	 */
  public passwordView:string =  'password'   

  /**
	 * Form group 
	 *
	 * @type { FormGroup }
	 */
  public createForm: FormGroup = new FormGroup({
        emailFormControl : new FormControl('', [Validators.required, Validators.email]),
        passwordFormControl : new FormControl('', [Validators.required]),
        nameFormControl : new FormControl('', [Validators.required])
  });

  /**
	 * Error checking
	 *
	 * @type { Function }
	 */
  public myError = (controlName: string, errorName: string) =>{
      return this.createForm.controls[controlName].hasError(errorName);
      }

  constructor(private router: Router, public loginService: LoginService,private _snackBar: MatSnackBar,) { }

  public setAll(event: boolean){
    this.passwordView = event ? 'text' : 'password'
  }

  public async back(){
    try{
       await this.router.navigate(['login'])
      }catch(error){
        console.error('error', error)
  
      }
  }

  public async create(){
    try{
        if (this.createForm.valid) {
          let loginUser = {
            name: this.createForm.value.nameFormControl,
            email: this.createForm.value.emailFormControl.trim(),
            password: this.createForm.value.passwordFormControl
          }
          await this.loginService.post(loginUser)
        }else{
          this._snackBar.open('OOOPS WE HAVE SOME ERRORS','', {
            duration: 5000,
            
          });
        }
      }catch(err: any){
        this._snackBar.open(err.error.message,'', {
          duration: 5000,
          panelClass: "warning-dialog"
        });
      }
  }

}
