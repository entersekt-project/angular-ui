import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../login/services/login.service';
import { HistoryService } from '../service/history.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {  
  
  /**
	 * History Columns
	 *
	 * @type { Array<string> }
	 */
  public displayedColumns: Array<string> = [
    'date',
    'time'
  ];

  /**
	 * User profile
	 *
	 * @type { {_id: string, name: string, email: string} }
	 */
  public userProfile: {_id?: string, name?: string, email?: string} = {}

  /**
	 * Array with the date and time of history
	 *
	 * @type { Array<{date: string, time:string}> }
	 */
  public dataSource:Array<{date: string, time:string}> = [];

  constructor(public historyService: HistoryService, private router: Router, private loginService: LoginService) { }
  
  async ngOnInit(): Promise<void> {
    let pipe = new DatePipe('en-US');
    this.userProfile = this.loginService.getActiveUser()
    let historyData  = await this.historyService.getById(this.userProfile._id)
    this.dataSource = historyData.map((entry: any)=>{ return {date: pipe.transform(new Date(entry.date), 'd MMMM , y'), time: pipe.transform(new Date(entry.date), 'h:mm a')}})
  }

  async logout(){    
    try{
      sessionStorage.removeItem("jwt");
      await this.router.navigate(['login'])
    }catch(error){
      console.log('error', error)

    }
  }
  
}
