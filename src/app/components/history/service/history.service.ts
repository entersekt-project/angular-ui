import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DatabaseBaseService } from 'src/app/helpers/database-base.service';

@Injectable({
  providedIn: 'root'
})
export class HistoryService extends DatabaseBaseService{
  public override root:string = 'loginHistory'
  constructor(public override http: HttpClient) { 
    super(http)
  }  

}
