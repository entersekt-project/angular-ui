import { Component } from '@angular/core';
import { LoginService } from 'src/app/components/login/services/login.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  /**
	 * Display password or text
	 *
	 * @type { boolean }
	 */
  public displayPassword: boolean = false;

  /**
	 * Input type
	 *
	 * @type { string }
	 */
  public passwordView:string =  'password'   

  /**
	 * Form group with validation
	 *
	 * @type { FormGroup }
	 */
  public loginForm: FormGroup = new FormGroup({
    emailFormControl : new FormControl('', [Validators.required, Validators.email]),
    passwordFormControl : new FormControl('', [Validators.required])
  });

  /**
	 * Error checking
	 *
	 * @type { Function }
	 */
  public myError = (controlName: string, errorName: string) =>{
    return this.loginForm.controls[controlName].hasError(errorName);
    }
  
  constructor(public loginService: LoginService, private _snackBar: MatSnackBar, private router: Router){} 

  public async login(){
    try{
      if (this.loginForm.valid) {
        let loginUser = {
          email: this.loginForm.value.emailFormControl.trim(),
          password: this.loginForm.value.passwordFormControl
        }
        await this.loginService.loginAuth(loginUser)
        await this.router.navigate(['history'])
      }else{
        this._snackBar.open('OOOPS WE HAVE SOME ERRORS','', {
          duration: 5000,
          panelClass: "warning-dialog"
        });
      }
    }catch(err: any){
      this._snackBar.open(err.error.message,'', {
        duration: 5000,
        panelClass: "warning-dialog"
      });
    }    
  }

  public async create(){
    try{
      await this.router.navigate(['create'])
    }catch(error){
      console.log('error', error)
    }
  }

  public setAll(event: boolean){
    this.passwordView = event ? 'text' : 'password'
  }
}
