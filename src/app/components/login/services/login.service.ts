import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API, ApiErrorResponseObject, DatabaseBaseService } from 'src/app/helpers/database-base.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService extends DatabaseBaseService{

  public override root:string = 'login'
  private _activeUser: {_id?: string, name?: string, email?: string} = {}

  constructor(public override http: HttpClient) { 
    super(http)
  }  
  
  public loginAuth(params: any){
    return this.http.post(`${API}/${this.root}/auth`, { params }).toPromise()
    .then((result: any) => {
      sessionStorage.setItem("jwt", result.token);
      this.setActiveUser(result)
      return result
    })
    .catch((err: ApiErrorResponseObject) => {
      console.log(err)
      throw err.error
    })
  }

  getActiveUser(){
    return this._activeUser
  }
  setActiveUser(detail: {_id?: string, name?: string, email?: string}){
    this._activeUser = detail
  }
}
