import { Injectable } from '@angular/core'
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import * as _ from 'lodash'
import { LoginService } from '../components/login/services/login.service'
@Injectable()
export class AuthGuard implements CanActivate {
	constructor(public router: Router,public loginService: LoginService) {}	
	async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
		
		try {
			let activeUser  =await this.loginService.getActiveUser()
			let jwt = sessionStorage.getItem("jwt");
            if((!activeUser || !jwt)){
                await this.stopLogin()
                return (!!activeUser && !!jwt)
            }
			return (!!activeUser && !!jwt)
		} catch (err) {
			await this.stopLogin()
			return false
		}
	}

	async stopLogin() {		
		await this.router.navigate(['/login'])
	}
}
