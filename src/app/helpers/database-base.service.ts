import { HttpClient } from '@angular/common/http';
import { Directive, Injectable } from '@angular/core';

export const API = '/api';

export class BaseDataObject{
  _id: string | undefined;
}
export class ApiErrorObject {
	public msg: string = 'OOPS'
}

export class ApiErrorResponseObject {
	public error: ApiErrorObject | undefined
	constructor(err?: ApiErrorObject) {
		this.error = err
	}
}
@Directive()
export abstract class  DatabaseBaseService {  
  public root: string = ''
  private apiRoot: string= `${API}/${this.root}`
  constructor(public http: HttpClient) { 
  }

  async get() {
    return this.http
    .get(`${API}/${this.root}`)
    .toPromise()
    .then((resp: any) => {
      return resp
    })
    .catch((err: ApiErrorResponseObject) => {
      throw err.error
    })
  }

  async getById(id:string = '') {
    return this.http
    .get(`${API}/${this.root}/${id}`)
    .toPromise()
    .then((resp: any) => {
      return resp
    })
    .catch((err: ApiErrorResponseObject) => {
      throw err.error
    })
  }

  async put(params = {}) {
    return this.http.put(`${API}/${this.root}`, { params }).toPromise()
    .then((resp: any) => {
      return resp
    })
    .catch((err: ApiErrorResponseObject) => {
      throw err.error
    })
  }

  async post(params = {}) {
    return this.http.post(`${API}/${this.root}`, { params }).toPromise()
    .then((resp: any) => {
      return resp
    })
    .catch((err: ApiErrorResponseObject) => {
      throw err.error
    })
  }
}
